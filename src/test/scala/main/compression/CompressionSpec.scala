package main.compression

import main.models.data.{Compressed, Repeat, Single}
import org.scalatest._

import scala.util.Random

class CompressionSpec extends WordSpec with Matchers {
  val compressor: Compressor = SequentialCompressor

  "the compressor" should {
    "compress and decompress a random sequence" in {
      val initialSequence = RLESequenceGenerator.generateChars
      val compressedSequence = compressor.compress(initialSequence)
      val decompressedSequence = compressor.decompress(compressedSequence)

      decompressedSequence shouldEqual initialSequence
    }

    "compress and decompress an empty sequence" in {
      val initialSequence = Nil

      val compressedSequence = compressor.compress(initialSequence)
      compressedSequence shouldBe empty
      val decompressedSequence = compressor.decompress(compressedSequence)
      decompressedSequence shouldBe empty
    }

    "compress and decompress a one-element sequence" in {
      val singleValue = "just me"
      val initialSequence = Seq(singleValue)

      val compressedSequence = compressor.compress(initialSequence)
      compressedSequence shouldBe Seq(Single(singleValue))
      val decompressedSequence = compressor.decompress(compressedSequence)
      decompressedSequence shouldBe Seq(singleValue)
    }

    "compress a sequence with one unique value occurring multiple times into a one-element sequence" in {
      val duplicationCount = 1000
      val duplicatedElement = "A"
      val duplicateElements = Seq.fill(duplicationCount)(duplicatedElement)

      val compressedSequence = compressor.compress(duplicateElements)

      compressedSequence shouldBe Seq(Repeat(count = duplicationCount, element = duplicatedElement))
    }

    "decompress a sequence with one compressed 'Repeat' element into a sequence with multiple duplicate elements" in {
      val duplicationCount = 1000
      val duplicatedElement = "A"
      val compressedSequence = Seq(Repeat(count = duplicationCount, element = duplicatedElement))

      val decompressedSequence = compressor.decompress(compressedSequence)

      decompressedSequence shouldEqual Seq.fill(duplicationCount)(duplicatedElement)
    }
  }

  "finding an element at index in a compressed sequence" should {
    "return the same element as if getting from the uncompressed sequence" in {
      val rng = new Random()
      val numberOfElements = rng.nextInt(1000)
      val uncompressedSequence = Seq.fill(numberOfElements)(rng.nextInt(10))
      val compressedSequence = compressor.compress(uncompressedSequence)

      0 until numberOfElements map { index =>
        Compressed.findAtIndex(compressedSequence, index).get shouldEqual uncompressedSequence(index)
      }
    }

    "return None if the sequence is empty" in {
      Compressed.findAtIndex(Nil, 0) shouldBe empty
    }

    "return None if the index is out of bounds" in {
      Compressed.findAtIndex(Seq(Single("just me")), 1) shouldBe empty
    }

    "return None if the index is negative" in {
      Compressed.findAtIndex(Nil, -1) shouldBe empty
    }
  }
}
