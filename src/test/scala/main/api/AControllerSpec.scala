package main.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import main.CachedFetcher
import org.scalamock.scalatest.MockFactory
import org.scalatest._

import scala.util.Random

class AControllerSpec extends WordSpec with Matchers with MockFactory with ScalatestRouteTest {
  "GET /A" should {
    "respond with a sequence of characters" in {
      val fetcher = mock[CachedFetcher]
      val route = new AController(fetcher).route

      Get("/A") ~> route ~> check {
        status shouldBe StatusCodes.OK
        val responseString = responseAs[String]
        responseString.split('\n').map(_.trim.length shouldBe 1)
      }
    }
  }

  "GET /{index}" should {
    "respond with OK and a character if a character is returned by the fetcher" in {
      val fetcher = mock[CachedFetcher]
      val route = new AController(fetcher).route

      val index = new Random().nextInt(1000)
      val character = 'A'

      (fetcher.findAtIndex _).expects(index).returning(Some(character))

      Get(s"/$index") ~> route ~> check {
        status shouldBe StatusCodes.OK
        val responseString = responseAs[String]
        responseString.length shouldBe 1
        responseString.head shouldBe character
      }
    }

    "respond with NotFound if a character is not found by the fetcher" in {
      val fetcher = mock[CachedFetcher]
      val route = new AController(fetcher).route

      val index = new Random().nextInt(1000)

      (fetcher.findAtIndex _).expects(index).returning(None)

      Get(s"/$index") ~> route ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }
  }
}
