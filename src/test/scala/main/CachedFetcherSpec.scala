package main

import java.util.concurrent.atomic.{AtomicBoolean, AtomicReference}

import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, StatusCodes}
import main.compression.{Compressor, SequentialCompressor}
import main.io.HttpClient
import main.models.conf.FetcherConf
import main.models.data.{Compressed, Repeat, Single}
import org.scalamock.scalatest.MockFactory
import org.scalatest._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


class CachedFetcherSpec extends WordSpec with Matchers with MockFactory {

  import Common.{as, ec, mat}

  "fetchAll" should {
    "return a 'Right' sequence of chars when the http client returns a correct response" in {
      val httpClient = mock[HttpClient]
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val fetcher = new CachedFetcher(
        httpClient = httpClient,
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf
      )

      (httpClient.execute _)
        .expects(
          where((req: HttpRequest) =>
            req.uri.toString() == fetcherConf.aUri &&
              req.method == HttpMethods.GET
          )
        )
        .returning(Future.successful(HttpResponse(entity = "A\nB\nB\nC\nC\nC\n")))

      val result = Await.result(fetcher.fetchAll(), 5.seconds)

      result.isRight shouldBe true

      val charSequence = result.getOrElse(throw new Exception)

      charSequence shouldEqual Seq('A', 'B', 'B', 'C', 'C', 'C')
    }

    "return a 'Left' httpResponse with status = BadGateway when the http client fails to return the expected response" in {
      val httpClient = mock[HttpClient]
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val fetcher = new CachedFetcher(
        httpClient = httpClient,
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf
      )

      (httpClient.execute _)
        .expects(*)
        .returning(Future.successful(HttpResponse(status = StatusCodes.InternalServerError)))

      val result = Await.result(fetcher.fetchAll(), 5.seconds)

      result.isLeft shouldBe true

      val errorResponse = result.swap.getOrElse(throw new Exception)

      errorResponse.status shouldBe StatusCodes.BadGateway
    }

    "fail when the httpClient fails to get any response" in {
      val httpClient = mock[HttpClient]
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val fetcher = new CachedFetcher(
        httpClient = httpClient,
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf
      )

      val exception = new Exception("No response")

      (httpClient.execute _)
        .expects(*)
        .returning(Future.failed(exception))

      val resultingException = Await.result(fetcher.fetchAll().failed, 5.seconds)

      resultingException shouldBe exception
    }
  }

  "getAll" should {
    "return the uncompressed cache data" in {
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val cache = new AtomicReference[Seq[Compressed[Char]]](
        Seq(
          Single(element = 'A'),
          Repeat(count = 2, element = 'B'),
          Repeat(count = 3, element = 'C')
        )
      )

      val fetcher = new CachedFetcher(
        httpClient = mock[HttpClient],
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf,
        cache = cache
      )

      fetcher.getAll shouldBe Seq('A', 'B', 'B', 'C', 'C', 'C')
    }

    "return an empty sequence when the cache is empty" in {
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val fetcher = new CachedFetcher(
        httpClient = mock[HttpClient],
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf
      )

      fetcher.getAll shouldBe Nil
    }

    "not invoke the httpClient" in {
      val httpClient = stub[HttpClient]
      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val fetcher = new CachedFetcher(
        httpClient = httpClient,
        compressor = SequentialCompressor,
        fetcherConf = fetcherConf
      )

      fetcher.getAll

      (httpClient.execute _).verify(*).never()
    }
  }

  "findAtIndex" should {
    "not decompress the compressed sequence" in {
      val decompressCalled = new AtomicBoolean(false)

      val compressor = new Compressor {
        override def compress[A]: Seq[A] => Seq[Compressed[A]] = ???

        override def decompress[A]: Seq[Compressed[A]] => Seq[A] = _ => {
          decompressCalled.set(true)
          Nil
        }
      }

      val fetcherConf = FetcherConf(
        aUri = "http://some-uri/abc",
        pollingInterval = 1.second
      )

      val cache = new AtomicReference[Seq[Compressed[Char]]](
        Seq(
          Single(element = 'A'),
          Repeat(count = 2, element = 'B'),
          Repeat(count = 3, element = 'C')
        )
      )

      val fetcher = new CachedFetcher(
        httpClient = mock[HttpClient],
        compressor = compressor,
        fetcherConf = fetcherConf,
        cache = cache
      )

      val result = fetcher.findAtIndex(3)

      result shouldBe Some('C')
      decompressCalled.get() shouldBe false
    }
  }
}
