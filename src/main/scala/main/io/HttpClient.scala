package main.io

import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

class HttpClient(http: HttpExt) extends LazyLogging {
  def execute(req: HttpRequest): Future[HttpResponse] = {
    logger.info(s"Executing request: $req")
    http.singleRequest(req)
  }
}
