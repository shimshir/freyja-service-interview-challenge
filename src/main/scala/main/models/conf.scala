package main.models

import scala.concurrent.duration.FiniteDuration

object conf {

  case class GlobalConf(app: AppConf)

  case class AppConf(server: ServerConf, fetcher: FetcherConf)

  case class ServerConf(port: Int)

  case class FetcherConf(aUri: String, pollingInterval: FiniteDuration)

}
