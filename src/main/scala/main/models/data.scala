package main.models

import scala.annotation.tailrec

object data {

  sealed trait Compressed[+A] {
    def count: Int

    def element: A

    def increased: Compressed[A]
  }

  object Compressed {
    def findAtIndex[A](elements: Seq[Compressed[A]], index: Int): Option[A] = {
      if (index < 0) {
        None
      } else {
        @tailrec
        def unfold(offset: Int, elements: Seq[Compressed[A]]): Option[A] = elements match {
          case Nil =>
            None
          case e +: es =>
            if (offset + e.count > index) {
              Some(e.element)
            } else {
              unfold(offset + e.count, es)
            }
        }

        unfold(0, elements)
      }
    }
  }

  case class Single[A](element: A) extends Compressed[A] {
    override def count: Int = 1

    override def increased: Compressed[A] = Repeat(count = 2, element = element)
  }

  case class Repeat[A](count: Int, element: A) extends Compressed[A] {
    override def increased: Compressed[A] = copy(count = count + 1)
  }

}
