package main

import java.util.concurrent.atomic.AtomicReference

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import main.compression.Compressor
import main.io.HttpClient
import main.models.conf.FetcherConf
import main.models.data.Compressed

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class CachedFetcher(
                     httpClient: HttpClient,
                     compressor: Compressor,
                     fetcherConf: FetcherConf,
                     cache: AtomicReference[Seq[Compressed[Char]]] = new AtomicReference[Seq[Compressed[Char]]](Nil)
                   )(implicit as: ActorSystem, mat: Materializer, ec: ExecutionContext) extends LazyLogging {

  /** A 'fetch' does not use the cache */
  def fetchAll(): Future[HttpResponse Either Seq[Char]] = {
    logger.info("Fetching all")
    httpClient
      .execute(HttpRequest(uri = fetcherConf.aUri))
      .flatMap(handleFetchAllResponse)
  }

  /** A 'get' uses the cache */
  def getAll: Seq[Char] = {
    logger.info("Decompressing cached compressed elements")
    compressor.decompress(cache.get())
  }

  /** A 'find' might not find a result, therefore it returns an Option */
  def findAtIndex(index: Int): Option[Char] = Compressed.findAtIndex(cache.get(), index)

  def startRepeatedFetch(): Unit = {
    val interval = fetcherConf.pollingInterval
    logger.info(s"Scheduling fetch in $interval intervals")
    as.scheduler.schedule(initialDelay = 0.seconds, interval = interval)(fetchAllCompressAndUpdateCache())
  }

  private def handleFetchAllResponse(res: HttpResponse): Future[HttpResponse Either Seq[Char]] = {
    res.status match {
      case StatusCodes.OK =>
        Unmarshal(res.entity).to[String]
          .map { resString =>
            val charSequence = resString.split("\n").map(_.trim.head).toSeq
            logger.info(s"Split the response into ${charSequence.size} characters")
            charSequence
          }.map(Right(_))
      case unexpectedResponseCode =>
        val errorMsg = s"Could not fetch As, response code: $unexpectedResponseCode"
        logger.error(errorMsg)
        Future.successful(
          Left(
            HttpResponse(
              status = StatusCodes.BadGateway,
              entity = HttpEntity(errorMsg)
            )
          )
        )
    }
  }

  private def fetchAllCompressAndUpdateCache(): Unit = {
    logger.info("Starting scheduled fetch")
    fetchAll().foreach {
      case Left(errorRes) =>
        logger.error(s"Failed to fetch all, response: $errorRes")
      case Right(characters) =>
        handleFetchedCharacters(characters)
    }
  }

  private def handleFetchedCharacters(uncompressedSequence: Seq[Char]): Unit = {
    logger.info(s"Compressing ${uncompressedSequence.size} elements")
    val compressedSequence = compressor.compress(uncompressedSequence)
    logger.info(s"Updating cache with ${compressedSequence.size} compressed elements")
    cache.set(compressedSequence)
  }
}
