package main

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import main.api.{AController, LoggedRoutes}
import main.compression.SequentialCompressor
import main.io.HttpClient
import main.models.conf.GlobalConf
import pureconfig.generic.auto._
import pureconfig.{ConfigReader, ConfigSource}

import scala.concurrent.ExecutionContext
import scala.util.Failure

object Main extends App with LazyLogging {
  implicitly[ConfigReader[GlobalConf]]

  val globalConf = ConfigSource.default.loadOrThrow[GlobalConf]

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val http = Http()

  val httpClient = new HttpClient(http)
  val fetcher = new CachedFetcher(
    httpClient = httpClient,
    compressor = SequentialCompressor,
    fetcherConf = globalConf.app.fetcher
  )

  val route = new AController(fetcher).route
  val loggedRoutes = LoggedRoutes(route)

  val port = globalConf.app.server.port

  http.bindAndHandle(handler = loggedRoutes, interface = "0.0.0.0", port = port)
    .onComplete {
      case Failure(t) =>
        logger.error("Failed to start the server, shutting down actor system", t)
        system.terminate()
      case _ =>
        logger.info(s"Server started successfully at port: $port, starting fetcher")
        fetcher.startRepeatedFetch()
    }
}
