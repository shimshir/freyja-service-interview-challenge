package main.api

import java.util.UUID

import akka.http.scaladsl.server.Directives.{extractRequest, mapResponse}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging

object LoggedRoutes extends LazyLogging {
  def apply(route: Route): Route =
    extractRequest { req =>
      val reqUuid = UUID.randomUUID().toString
      logger.debug(s"Request '$reqUuid':\n$req")
      mapResponse { res =>
        val resWithOmittedEntity = res.mapEntity(_ => "<< omitted >>")
        logger.debug(s"Response to req '$reqUuid':\n$resWithOmittedEntity")
        if (res.entity.isStrict()) {
          logger.trace(s"Response entity for req '$reqUuid':\n${res.entity}")
        }
        res
      }(route)
    }
}
