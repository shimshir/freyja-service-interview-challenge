package main.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import main.CachedFetcher
import main.compression.RLESequenceGenerator

class AController(fetcher: CachedFetcher) {
  def route: Route = getA ~ getAAtIndex

  private def getA: Route =
    pathPrefix("A") {
      pathEndOrSingleSlash {
        get {
          complete(RLESequenceGenerator.generateChars.mkString("\n"))
        }
      }
    }

  /** I would rather have a path schema like /A/{index} for this route, but I am following the requirements here */
  private def getAAtIndex: Route =
    pathPrefix(IntNumber) { index =>
      pathEndOrSingleSlash {
        get {
          fetcher.findAtIndex(index) match {
            case None =>
              complete(StatusCodes.NotFound, s"There seems to be no element at index: $index")
            case Some(char) =>
              complete(char.toString)
          }
        }
      }
    }
}
