package main.compression

import main.models.data.{Compressed, Single}

trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]

  def decompress[A]: Seq[Compressed[A]] => Seq[A]
}

/**
 * This compressor iterates over the sequence sequentially.
 *
 * I imagine that it would be possible to compress the sequence in parallel as well by using a map-reduce approach.
 *
 * The whole sequence could be split into multiple groups, those groups could be compressed in parallel, then
 * the compressed groups would be combined back again into one sequence of compressed items,
 * the tricky part would be joining the last and first element of adjacent groups.
 *
 * Of course, such an implementation would only make sense when compressing very, very, large sequences,
 * but I thought I'd mention it.
 */
object SequentialCompressor extends Compressor {
  override def compress[A]: Seq[A] => Seq[Compressed[A]] = {
    case Nil => Nil
    case a +: as =>
      as.foldLeft[Seq[Compressed[A]]](Seq(Single(a))) {
        case (compressedElements@c +: cs, anA) =>
          if (c.element == anA) {
            c.increased +: cs
          } else {
            Single(anA) +: compressedElements
          }
      }.reverse
  }

  override def decompress[A]: Seq[Compressed[A]] => Seq[A] =
    _.flatMap(c => Seq.fill(c.count)(c.element))
}
