package main.compression

import scala.util.Random

object RLESequenceGenerator {

  val sequences = 10
  val maxSeqLen = 10000
  val maxSectionSize = maxSeqLen / sequences

  def generateChars: List[Char] = {
    def getNewRepeatsCount: Int = Random.nextInt(maxSectionSize) + 1

    def getNewChar: Char = (Random.nextInt(25) + 65).toChar // A - Z

    List.fill(sequences)(getNewChar).flatMap(c => List.fill(getNewRepeatsCount)(c))
  }
}
