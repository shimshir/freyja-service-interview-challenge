ThisBuild / scalaVersion := "2.13.1"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "free2move"
ThisBuild / organizationName := "free2move"

maintainer := "me.admir@gmail.com"

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "freyja-service-interview-challenge",
    libraryDependencies ++=
      Seq(
        "com.typesafe.akka" %% "akka-http" % "10.1.10",
        "com.typesafe.akka" %% "akka-stream" % "2.5.25",
        "com.typesafe.akka" %% "akka-slf4j" % "2.5.25",
        "com.github.pureconfig" %% "pureconfig" % "0.12.1",
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
        "org.scalatest" %% "scalatest" % "3.0.8" % Test,
        "org.scalamock" %% "scalamock" % "4.4.0" % Test,
        "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.25" % Test,
        "com.typesafe.akka" %% "akka-http-testkit" % "10.1.10" % Test
      )
  )

ThisBuild / scalacOptions ++= Seq("-unchecked", "-deprecation")
