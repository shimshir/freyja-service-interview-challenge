## Test
`sbt test`

## Run
`sbt run` runs it on port `8080` by default

`APP_SERVER_PORT=<custom-port> sbt run` runs it on `<custom-port>`

**Remarks:**  
I took some liberty in updating and changing some of the dependencies and tooling. Most of the new dependencies
I introduced are "quality of life" libraries that make it easy to get a logger or load the configuration for example
and testing libraries, I hope that is ok.

I am also not providing a user guide on how to package a distribution zip or dockerize the application as all of that
can be found on https://www.scala-sbt.org/sbt-native-packager

I changed the initial package structure to make it more sensible to reason as to what the classes inside the packages do.
